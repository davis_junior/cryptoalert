import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import bittrex.GetMarkets;
import bittrex.GetTicker;
import bittrex.Market;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class CurrencyChooser extends Dialog {
	
	private static DecimalFormat df8 = new DecimalFormat(".00000000");

	protected Object result = null;
	private Alert alert;
	
	protected Shell shell;
	private Text textFilter;
	private List list;
	
	GetMarkets getMarkets;
	private Label lblPriceAbove;
	private Text textAbove;
	private Label lblCurrentPrice;
	private Label lblPriceBelow;
	private Text textBelow;
	private Button btnSave;
	private Button btnCancel;
	private Label lblCurrency;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public CurrencyChooser(Shell parent, int style) {
		super(parent, style);
		setText("Currency Chooser");
		
		getMarkets = new GetMarkets();
		getMarkets.run();
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		
		for (Market market : getMarkets.getResult()) {
			list.add(market.getMarketName() + " - " + market.getMarketCurrencyLong());
		}
		
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(269, 472);
		shell.setText(getText());
		
		textFilter = new Text(shell, SWT.BORDER);
		textFilter.setBounds(61, 7, 192, 29);
		
		ModifyListener modifyListener = new ModifyListener() {
		    public void modifyText(ModifyEvent e) {
		    	list.removeAll();
				for (Market market : getMarkets.getResult()) {
					if (market.getMarketName().toLowerCase().contains(textFilter.getText().toLowerCase())
							|| market.getMarketCurrencyLong().toLowerCase().contains(textFilter.getText().toLowerCase()))
					list.add(market.getMarketName() + " - " + market.getMarketCurrencyLong());
				}
		    }
		};
		textFilter.addModifyListener(modifyListener);
		
		Label lblFilter = new Label(shell, SWT.NONE);
		lblFilter.setBounds(10, 10, 45, 23);
		lblFilter.setText("Filter");
		
		list = new List(shell, SWT.BORDER | SWT.V_SCROLL);
		list.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String selection = list.getItem(list.getSelectionIndex());
				String marketName = selection.split(" - ")[0];
				String marketCurrencyLong = selection.split(" - ")[1];
				//System.out.println(marketName + ", " + marketCurrencyLong);
				
				alert = new Alert(marketName, marketCurrencyLong, -1, -1, false, false);
				
				lblCurrency.setText("Currency: " + selection);
				
				GetTicker getTicker = new GetTicker(marketName);
				getTicker.run();
				if (getTicker.isSuccess()) {
					double last = getTicker.getResult().getLast();
					alert.setAbove(last);
					alert.setBelow(last);
					lblCurrentPrice.setText("Current Price: " + df8.format(last));
					textAbove.setText(df8.format(last));
					textBelow.setText(df8.format(last));
					alert.setAboveEnabled(false);
					alert.setBelowEnabled(false);
				}
			}
		});
		list.setBounds(10, 42, 243, 193);
		
		lblPriceAbove = new Label(shell, SWT.NONE);
		lblPriceAbove.setBounds(10, 311, 135, 23);
		lblPriceAbove.setText("Price at or Above:");
		
		textAbove = new Text(shell, SWT.BORDER);
		textAbove.setBounds(151, 308, 89, 29);
		ModifyListener modifyListener2 = new ModifyListener() {
		    public void modifyText(ModifyEvent e) {
		    	if (alert != null) {
		    		alert.setAbove(Double.parseDouble(textAbove.getText()));
		    		if (alert.getAbove() > 0)
		    			alert.setAboveEnabled(true);
		    	}
		    }
		};
		textAbove.addModifyListener(modifyListener2);
		
		lblCurrentPrice = new Label(shell, SWT.NONE);
		lblCurrentPrice.setBounds(10, 282, 243, 23);
		lblCurrentPrice.setText("Current Price: ");
		
		lblPriceBelow = new Label(shell, SWT.NONE);
		lblPriceBelow.setBounds(10, 343, 135, 23);
		lblPriceBelow.setText("Price at or Below:");
		
		textBelow = new Text(shell, SWT.BORDER);
		textBelow.setBounds(151, 340, 89, 29);
		ModifyListener modifyListener3 = new ModifyListener() {
		    public void modifyText(ModifyEvent e) {
		    	if (alert != null) {
		    		alert.setBelow(Double.parseDouble(textBelow.getText()));
		    		if (alert.getBelow() > 0)
		    			alert.setBelowEnabled(true);
		    	}
		    }
		};
		textBelow.addModifyListener(modifyListener3);
		
		btnSave = new Button(shell, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (alert != null && (alert.isAboveEnabled() || alert.isBelowEnabled()))
					result = alert;
				else
					result = null;
				
				shell.close();
			}
		});
		btnSave.setBounds(10, 394, 103, 33);
		btnSave.setText("Save");
		
		btnCancel = new Button(shell, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = null;
				shell.close();
			}
		});
		btnCancel.setBounds(150, 394, 103, 33);
		btnCancel.setText("Cancel");
		
		lblCurrency = new Label(shell, SWT.NONE);
		lblCurrency.setBounds(10, 253, 230, 23);
		lblCurrency.setText("Currency: ");
	}
}
