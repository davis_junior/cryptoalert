import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bittrex.AbstractRequest;
import bittrex.GetMarketHistory;
import bittrex.GetMarketSummaries;
import bittrex.GetMarkets;
import bittrex.GetTicker;
import bittrex.Market;
import bittrex.MarketHistory;
import bittrex.MarketSummary;
import bittrex.Ticker;
import coinbase.GetSpotPrice;
import coinbase.Price;
import gson.DateDeserializer;

import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;

public class Frame {
	
	private static DecimalFormat df0 = new DecimalFormat("0");
	private static DecimalFormat df1 = new DecimalFormat(".0");
	private static DecimalFormat df2 = new DecimalFormat(".00");
	private static DecimalFormat df02 = new DecimalFormat("0.00");
	private static DecimalFormat df8 = new DecimalFormat(".00000000");
	
	private Gson gson;
	private HttpRequestFactory requestFactory;
	
	private Label lblBtc;
	private Label lblCurrency;
	private Label lblLastPrice;
	private org.eclipse.swt.widgets.List listAlertNotifications;

	protected Shell shell;
	private Table table;
	private Tree tree;
	
	private Alert btcAlert;
	private Object btcAlertLock = new Object();
	private List<Alert> alerts = new ArrayList<>();
	private Object alertLock = new Object();
	
	private List<Alert> alertNotifications = new ArrayList<>();

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Frame window = new Frame();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Frame() {
        gson = new GsonBuilder()
        		//.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
        		.setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        		.registerTypeAdapter(Date.class, new DateDeserializer())
        		.setFieldNamingStrategy(new FieldNamingStrategy() {
			
			@Override
			public String translateName(Field field) {
		        String fieldName = FieldNamingPolicy.IDENTITY.translateName(field);
	            if (field.getDeclaringClass().equals(GetSpotPrice.class) 
	            		|| field.getDeclaringClass().equals(Price.class) 
	            		|| field.getDeclaringClass().isAssignableFrom(AbstractRequest.class)) {
	            	fieldName = FieldNamingPolicy.IDENTITY.translateName(field);
	            	
	            } else
	            	fieldName = FieldNamingPolicy.UPPER_CAMEL_CASE.translateName(field);
	            
	            return fieldName;
			}
		}).create();
        
		requestFactory = new NetHttpTransport().createRequestFactory();
		
		btcAlert = new Alert("BTC", "BTC", -1, -1, false, false);
	}

	public void test() {
		/*GetMarkets getMarkets = new GetMarkets();
		getMarkets.run();
        
        System.out.println(getMarkets.isSuccess());
        System.out.println(getMarkets.getMessage());
        
        ArrayList<Market> markets = getMarkets.getResult();
        if (getMarkets.isSuccess()) {
        	for (Market market : markets) {
	        	System.out.println(market.getMarketCurrencyLong());
        	}
        }*/
		
		GetMarketHistory getMarketHistory = new GetMarketHistory("BTC-DOGE");
		getMarketHistory.run();
		ArrayList<MarketHistory> marketHistory = getMarketHistory.getResult();
        if (getMarketHistory.isSuccess()) {
        	for (MarketHistory history : marketHistory) {
	        	System.out.println(history.getTimeStamp());
        	}
        }
        
        //System.exit(0);
	}
	
	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		
		test();
		
		Thread threadTopTab = new Thread() {
			public void run() {
				while (!shell.isDisposed()) {
					double btcValue = 0;
					try {
						HttpRequest request = requestFactory.buildGetRequest(new GenericUrl("https://api.coinbase.com/v2/prices/BTC-USD/spot"));
						String rawResponse = request.execute().parseAsString();
						GetSpotPrice getSpotPrice = gson.fromJson(rawResponse, GetSpotPrice.class);
				        
						btcValue = getSpotPrice.getData().getAmount();
						//System.out.println("BTC = $" + btcValue);
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					final double fBtcValue = btcValue;
					Display.getDefault().asyncExec(new Runnable() {
						 public void run() {
							 lblBtc.setText("(BTC) $" + df0.format(fBtcValue));
							 shell.setText("(BTC) $" + df0.format(fBtcValue));
						 }
					});
					
					
					GetMarketSummaries getMarketSummaries = new GetMarketSummaries();
					getMarketSummaries.run();
					
					GetMarkets getMarkets = new GetMarkets();
					getMarkets.run();
					
					if (getMarketSummaries.isSuccess()) {
						ArrayList<MarketSummary> marketSummarys = getMarketSummaries.getResult();
						
						// sort on volume
						Collections.sort(marketSummarys, new Comparator<MarketSummary>(){
						     public int compare(MarketSummary o1, MarketSummary o2){
						         if (o1.getBaseVolume() == o2.getBaseVolume())
						             return 0;
						         
						         return o1.getBaseVolume() > o2.getBaseVolume() ? -1 : 1;
						     }
						});
						
						// sort on name
						Collections.sort(marketSummarys, new Comparator<MarketSummary>(){
						     public int compare(MarketSummary o1, MarketSummary o2){
						         if (o1.getMarketName().startsWith("BTC-") && o2.getMarketName().startsWith("BTC-"))
						             return 0;
						         
						         if (o1.getMarketName().startsWith("BTC-"))
						        	 return -1;
						         else
						        	 return 1;
						     }
						});
						
						/*Display.getDefault().asyncExec(new Runnable() {
							 public void run() {
								 table.removeAll();
							 }
						});*/
						
						int tiIndex = 0;
					    for (MarketSummary marketSummary : marketSummarys) {
					    	if (marketSummary.getMarketName().startsWith("BTC-") || marketSummary.getMarketName().startsWith("USDT-")) {
					    		double percentage = 0;
					    		if (marketSummary.getLast() == marketSummary.getPrevDay())
					    			percentage = 0;
					    		else
					    			percentage = (marketSummary.getLast() / marketSummary.getPrevDay() - 1) * 100;
					    		
					    		String name = null;
					    		for (Market market : getMarkets.getResult()) {
					    			if (market.getMarketName().equals(marketSummary.getMarketName())) {
					    				name = market.getMarketCurrencyLong();
					    				break;
					    			}
					    		}
					    		
					    		String last = null;
					    		if (marketSummary.getMarketName().startsWith("BTC-"))
					    			last = df8.format(marketSummary.getLast());
					    		else
					    			last = "$" + df02.format(marketSummary.getLast());
					    		
					    		final String fName = name;
					    		final int fTiIndex = tiIndex;
					    		final double fPercentage = percentage;
					    		final String fLast = last;
								Display.getDefault().asyncExec(new Runnable() {
									 public void run() {
										 TableItem ti;
										 if (fTiIndex < table.getItemCount())
											 ti = table.getItem(fTiIndex);
										 else
											 ti = new TableItem(table, SWT.None);
										 
										 ti.setText(new String[] { marketSummary.getMarketName(), fName, "" + Math.round(marketSummary.getBaseVolume()), df1.format(fPercentage) + "%", fLast });
										 
										 if (fPercentage >= 0)
											 ti.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
										 else
											 ti.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
									 }
								});
					    		
					    		//System.out.println(marketSummary.getMarketName() + " - " + Math.round(marketSummary.getBaseVolume()) + " - " + df1.format(percentage) + "%" + " - " + df8.format(marketSummary.getLast()));
								
								tiIndex++;
					    	}
					    }
					    
					    // remove old table items
					    final int fTiIndex = tiIndex;
						Display.getDefault().asyncExec(new Runnable() {
						 public void run() {
							 if (fTiIndex < table.getItemCount()) {
								 table.remove(fTiIndex, table.getItemCount() - 1);
							 }
						 }
						});
					}
					
					
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		threadTopTab.start();
		
		
		Thread threadAlerts = new Thread() {
			public void run() {
				while (!shell.isDisposed()) {
					synchronized (btcAlertLock) {
						if (btcAlert.isAboveEnabled() || btcAlert.isBelowEnabled()) {
							double btcValue = 0;
							try {
								HttpRequest request = requestFactory.buildGetRequest(new GenericUrl("https://api.coinbase.com/v2/prices/BTC-USD/spot"));
								String rawResponse = request.execute().parseAsString();
								GetSpotPrice getSpotPrice = gson.fromJson(rawResponse, GetSpotPrice.class);
						        
								btcValue = getSpotPrice.getData().getAmount();
								//System.out.println("BTC = $" + btcValue);
							} catch (IOException e) {
								e.printStackTrace();
							}
							
							if (btcAlert.isAboveEnabled()) {
								if (btcValue >= btcAlert.getAbove()) {
									if (!alertNotifications.contains(btcAlert))
										alertNotifications.add(btcAlert);
									btcAlert.setAboveTimestamp(System.currentTimeMillis());
									System.out.println("Alert above: " + btcAlert.getMarketName() + " --> $" + df0.format(btcValue));
									
									btcAlert.setAboveEnabled(false);
									
									final double fBtcValue = btcValue;
									Display.getDefault().asyncExec(new Runnable() {
										 public void run() {
											 listAlertNotifications.add("Up signal - " + btcAlert.getMarketName() + "  is up --> $" + df0.format(fBtcValue), 0);
											 listAlertNotifications.add("  - just now", 1);
											 showBaloonMessage("Up signal - " + btcAlert.getMarketName(), btcAlert.getMarketCurrencyLong() + " is up --> $" + df0.format(fBtcValue));
											 playAlert();
										 }
										});
								}
							}
							
							if (btcAlert.isBelowEnabled()) {
								if (btcValue <= btcAlert.getBelow()) {
									if (!alertNotifications.contains(btcAlert))
										alertNotifications.add(btcAlert);
									btcAlert.setBelowTimestamp(System.currentTimeMillis());
									System.out.println("Alert below: " + btcAlert.getMarketName() + " --> $" + df0.format(btcValue));
									
									btcAlert.setBelowEnabled(false);
									
									final double fBtcValue = btcValue;
									Display.getDefault().asyncExec(new Runnable() {
										 public void run() {
											 listAlertNotifications.add("Down signal - " + btcAlert.getMarketName() + "  is down --> $" + df0.format(fBtcValue), 0);
											 listAlertNotifications.add("  - just now", 1);
											 showBaloonMessage("Down signal - " + btcAlert.getMarketName(), btcAlert.getMarketCurrencyLong() + " is down --> $" + df0.format(fBtcValue));
											 playAlert();
										 }
										});
								}
							}
						}
					}
					
					synchronized (alertLock) {
						if (alerts.size() > 0) {
							for (Alert alert : alerts) {
								if (alert.isAboveEnabled() || alert.isBelowEnabled()) {
									GetTicker getTicker = new GetTicker(alert.getMarketName());
									getTicker.run();
									Ticker ticker = getTicker.getResult();
									
									if (getTicker.isSuccess()) {
										if (alert.isAboveEnabled()) {
											if (ticker.getLast() >= alert.getAbove()) {
												if (!alertNotifications.contains(alert))
													alertNotifications.add(alert);
												alert.setAboveTimestamp(System.currentTimeMillis());
												System.out.println("Alert above: " + alert.getMarketName() + " --> " + df8.format(ticker.getLast()));
												
												alert.setAboveEnabled(false);
												
												Display.getDefault().asyncExec(new Runnable() {
													 public void run() {
														 for (TreeItem ti : tree.getItems()) {
															 if (ti.getData() == alert) {
																 ti.getItem(0).setChecked(false);
																 if (!ti.getItem(1).getChecked())
																	 ti.setChecked(false);
															 }
														 }
														 
														 listAlertNotifications.add("Up signal - " + alert.getMarketName() + "  is up --> " + df8.format(ticker.getLast()), 0);
														 listAlertNotifications.add("  - just now", 1);
														 showBaloonMessage("Up signal - " + alert.getMarketName(), alert.getMarketCurrencyLong() + " is up --> " + df8.format(ticker.getLast()));
														 playAlert();
													 }
													});
											}
										}
										
										if (alert.isBelowEnabled()) {
											if (ticker.getLast() <= alert.getBelow()) {
												if (!alertNotifications.contains(alert))
													alertNotifications.add(alert);
												alert.setBelowTimestamp(System.currentTimeMillis());
												System.out.println("Alert below: " + alert.getMarketName() + " --> " + df8.format(ticker.getLast()));
												
												alert.setBelowEnabled(false);
												Display.getDefault().asyncExec(new Runnable() {
													 public void run() {
														 for (TreeItem ti : tree.getItems()) {
															 if (ti.getData() == alert) {
																 ti.getItem(1).setChecked(false);
																 if (!ti.getItem(0).getChecked())
																	 ti.setChecked(false);
															 }
														 }
														 
														 listAlertNotifications.add("Down signal - " + alert.getMarketName() + "  is down --> " + df8.format(ticker.getLast()), 0);
														 listAlertNotifications.add("  - just now", 1);
														 showBaloonMessage("Down signal - " + alert.getMarketName(), alert.getMarketCurrencyLong() + " is down --> " + df8.format(ticker.getLast()));
														 playAlert();
													 }
													});
											}
										}
									}
								}
							}
							
							// update last price on tab
							Display.getDefault().asyncExec(new Runnable() {
								 public void run() {
									if (tree.getSelectionCount() > 0) {
										for (TreeItem ti : tree.getSelection()) {
											Alert alert = (Alert) ti.getData();
											if (alert == null) {
												if (ti.getParentItem() != null) {
													alert = (Alert) ti.getParentItem().getData();
												}
											}
											
											if (alert != null) {
												GetTicker getTicker = new GetTicker(alert.getMarketName());
												getTicker.run();
												if (getTicker.isSuccess()) {
													lblCurrency.setText("Currency: " + alert.getMarketName() + " - " + alert.getMarketCurrencyLong());
													lblLastPrice.setText("Last: " + df8.format(getTicker.getResult().getLast()));
												}
											}
										}
									}
							}});
						}
					}
					
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		threadAlerts.start();
		
		Thread threadAlertNotifications = new Thread() {
			public void run() {
				while (!shell.isDisposed()) {
					// refresh notification times
					if (alertNotifications.size() > 0) {
						Collections.sort(alertNotifications, new Comparator<Alert>() {
							@Override
							public int compare(final Alert lhs, Alert rhs) {
								if (lhs.getAboveTimestamp() < rhs.getAboveTimestamp())
									return 1;
								else if (lhs.getAboveTimestamp() > rhs.getAboveTimestamp())
									return -1;
								
								if (lhs.getBelowTimestamp() < rhs.getBelowTimestamp())
									return 1;
								else if (lhs.getBelowTimestamp() > rhs.getBelowTimestamp())
									return -1;
								
								return 0;
							}
						});
						
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								listAlertNotifications.removeAll();
								
								for (Alert alert : alertNotifications) {
									if (alert.equals(btcAlert)) {
										double btcValue = 0;
										try {
											HttpRequest request = requestFactory.buildGetRequest(new GenericUrl("https://api.coinbase.com/v2/prices/BTC-USD/spot"));
											String rawResponse = request.execute().parseAsString();
											GetSpotPrice getSpotPrice = gson.fromJson(rawResponse, GetSpotPrice.class);
									        
											btcValue = getSpotPrice.getData().getAmount();
											//System.out.println("BTC = $" + btcValue);
										} catch (IOException e) {
											e.printStackTrace();
										}
										
										final double fBtcValue = btcValue;
										
										if (alert.getAboveTimestamp() != -1) {
											listAlertNotifications.add("Up signal - " + alert.getMarketName() + "  is up --> $" + df0.format(fBtcValue));
											int minutes = Math.toIntExact((System.currentTimeMillis() - alert.getAboveTimestamp()) / 1000 / 60);
											if (minutes > 0)
												listAlertNotifications.add("  - " + minutes + " mintues ago");
											else
												listAlertNotifications.add("  - < 1 mintue ago");
										}
										
										if (alert.getBelowTimestamp() != -1) {
											listAlertNotifications.add("Down signal - " + alert.getMarketName() + "  is down --> $" + df0.format(fBtcValue));
											int minutes = Math.toIntExact((System.currentTimeMillis() - alert.getAboveTimestamp()) / 1000 / 60);
											if (minutes > 0)
												listAlertNotifications.add("  - " + ((System.currentTimeMillis() - alert.getBelowTimestamp()) / 1000 / 60) + " mintues ago");
											else
												listAlertNotifications.add("  - < 1 mintue ago");
										}
										
									} else {
										GetTicker getTicker = new GetTicker(alert.getMarketName());
										getTicker.run();
										Ticker ticker = getTicker.getResult();
										
										if (alert.getAboveTimestamp() != -1) {
											listAlertNotifications.add("Up signal - " + alert.getMarketName() + "  is up --> " + df8.format(ticker.getLast()));
											int minutes = Math.toIntExact((System.currentTimeMillis() - alert.getAboveTimestamp()) / 1000 / 60);
											if (minutes > 0)
												listAlertNotifications.add("  - " + minutes + " mintues ago");
											else
												listAlertNotifications.add("  - < 1 mintue ago");
										}
										
										if (alert.getBelowTimestamp() != -1) {
											listAlertNotifications.add("Down signal - " + alert.getMarketName() + "  is down --> " + df8.format(ticker.getLast()));
											int minutes = Math.toIntExact((System.currentTimeMillis() - alert.getAboveTimestamp()) / 1000 / 60);
											if (minutes > 0)
												listAlertNotifications.add("  - " + ((System.currentTimeMillis() - alert.getBelowTimestamp()) / 1000 / 60) + " mintues ago");
											else
												listAlertNotifications.add("  - < 1 mintue ago");
										}
									}
								}
						}});
					}
					
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		threadAlertNotifications.start();
				
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	private void showBaloonMessage(String title, String message) {
	    //Image image = null;
	    final ToolTip tip = new ToolTip(shell, SWT.BALLOON | SWT.ICON_INFORMATION);
	    tip.setMessage(message);
	    Tray tray = Display.getCurrent().getSystemTray();
	    if (tray != null) {
	      TrayItem item = new TrayItem(tray, SWT.NONE);
	      //image = new Image(Display.getCurrent(), "yourFile.gif");
	      //item.setImage(image);
	      tip.setText(title);
	      item.setToolTip(tip);
	    } else {
	      tip.setText(title);
	      tip.setLocation(400, 400);
	    }
	    
	    tip.setVisible(true);
	}
	
	private void playAlert() {
		Thread thread = new Thread() {
			public void run() {
				try {
					AudioInputStream audioStream = AudioSystem.getAudioInputStream(getClass().getResourceAsStream("/res/alert.wav"));
					AudioFormat format = audioStream.getFormat();
					 
					DataLine.Info info = new DataLine.Info(Clip.class, format);
					Clip audioClip = (Clip) AudioSystem.getLine(info);
					audioClip.open(audioStream);
					audioClip.start();
					
					Thread.sleep(5000);
					audioClip.close();
					audioStream.close();
				} catch (UnsupportedAudioFileException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (LineUnavailableException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		thread.start();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(675, 462);
		shell.setText("$0");
		
		TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
		tabFolder.setBounds(10, 10, 637, 399);
		
		TabItem tbtmTop = new TabItem(tabFolder, SWT.NONE);
		tbtmTop.setText("Top");
		
		Group group = new Group(tabFolder, SWT.NONE);
		tbtmTop.setControl(group);
		
		lblBtc = new Label(group, SWT.BORDER);
		lblBtc.setBounds(10, 20, 154, 23);
		lblBtc.setAlignment(SWT.CENTER);
		lblBtc.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		lblBtc.setText("(BTC) $0");
		
		table = new Table(group, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(10, 58, 609, 302);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnMarket = new TableColumn(table, SWT.NONE);
		tblclmnMarket.setWidth(100);
		tblclmnMarket.setText("Market");
		
		TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(203);
		tblclmnName.setText("Name");
		
		TableColumn tblclmnVolume = new TableColumn(table, SWT.NONE);
		tblclmnVolume.setWidth(100);
		tblclmnVolume.setText("Volume");
		
		TableColumn tblclmnChange = new TableColumn(table, SWT.NONE);
		tblclmnChange.setWidth(81);
		tblclmnChange.setText("Change");
		
		TableColumn tblclmnLast = new TableColumn(table, SWT.NONE);
		tblclmnLast.setWidth(100);
		tblclmnLast.setText("Last");
		
		Button btnCreateBtcAlert = new Button(group, SWT.NONE);
		btnCreateBtcAlert.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				BTCAlert btcAlertDialog = new BTCAlert(shell, SWT.NONE, btcAlert);
				synchronized (btcAlertLock) {
					Alert alert = (Alert) btcAlertDialog.open();
					if (alert != null) {
						btcAlert = alert;
					}
				}
			}
		});
		btnCreateBtcAlert.setBounds(182, 15, 154, 33);
		btnCreateBtcAlert.setText("Create BTC Alert");
		
		TabItem tbtmAlerts = new TabItem(tabFolder, SWT.NONE);
		tbtmAlerts.setText("Alerts");
		
		Group group_1 = new Group(tabFolder, SWT.NONE);
		tbtmAlerts.setControl(group_1);
		
		Button btnAddAlert = new Button(group_1, SWT.NONE);
		btnAddAlert.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CurrencyChooser currencyChooser = new CurrencyChooser(shell, SWT.NONE);
				Alert alert = (Alert) currencyChooser.open();
				if (alert != null) {
					synchronized (alertLock) {
						alerts.add(alert);
					}
					
					TreeItem ti = new TreeItem(tree, SWT.NONE);
					ti.setData(alert);
					ti.setText(alert.getMarketName() + " - " + alert.getMarketCurrencyLong());
					
					TreeItem tiAbove = new TreeItem(ti, SWT.NONE);
					tiAbove.setText("At or above " + df8.format(alert.getAbove()));
					tiAbove.setChecked(alert.isAboveEnabled());
					
					TreeItem tiBelow = new TreeItem(ti, SWT.NONE);
					tiBelow.setText("At or below " + df8.format(alert.getBelow()));
					tiBelow.setChecked(alert.isBelowEnabled());
					
					ti.setExpanded(true);
					if (alert.isAboveEnabled() || alert.isBelowEnabled())
						ti.setChecked(true);
				}
			}
		});
		btnAddAlert.setBounds(278, 281, 103, 33);
		btnAddAlert.setText("Add Alert");
		
		tree = new Tree(group_1, SWT.BORDER | SWT.CHECK);
		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.detail == SWT.CHECK) {
					if (tree.getItemCount() > 0) {
						for (TreeItem ti : tree.getItems()) {
							Alert alert = (Alert) ti.getData();
							alert.setAboveEnabled(ti.getItem(0).getChecked());
							alert.setBelowEnabled(ti.getItem(1).getChecked());
							
							boolean anyChecked = false;
							for (TreeItem ti2 : ti.getItems()) {
								if (ti2.getChecked()) {
									anyChecked = true;
									ti.setChecked(true);
								}
							}
							if (!anyChecked)
								ti.setChecked(false);
						}
					}
				}
			}
		});
		tree.setBounds(10, 49, 262, 304);
		
		Button btnRemove = new Button(group_1, SWT.NONE);
		btnRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (tree.getSelectionCount() > 0) {
					for (TreeItem ti : tree.getSelection()) {
						Alert alert = (Alert) ti.getData();
						if (alert != null) {
							synchronized (alertLock) {
								alerts.remove(alert);
							}
							ti.dispose();
						}
					}
				}
			}
		});
		btnRemove.setBounds(278, 320, 103, 33);
		btnRemove.setText("Remove");
		
		lblCurrency = new Label(group_1, SWT.NONE);
		lblCurrency.setBounds(10, 20, 262, 23);
		lblCurrency.setText("Currency: ");
		
		lblLastPrice = new Label(group_1, SWT.NONE);
		lblLastPrice.setBounds(278, 20, 138, 23);
		lblLastPrice.setText("Last:");
		
		Label lblNotifications = new Label(group_1, SWT.NONE);
		lblNotifications.setBounds(278, 49, 138, 20);
		lblNotifications.setText("Notifications:");
		
		listAlertNotifications = new org.eclipse.swt.widgets.List(group_1, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		listAlertNotifications.setBounds(278, 75, 341, 200);
		
		TabItem tbtmScanner = new TabItem(tabFolder, SWT.NONE);
		tbtmScanner.setText("Scanner");
	}
}
