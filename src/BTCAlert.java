import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bittrex.AbstractRequest;
import coinbase.GetSpotPrice;
import coinbase.Price;
import gson.DateDeserializer;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class BTCAlert extends Dialog {
	
	private static DecimalFormat df0 = new DecimalFormat("0");

	private Alert alert;
	protected Object result;
	
	private double btcValue = 0;
	
	protected Shell shell;
	private Text textAbove;
	private Text textBelow;
	private Label lblCurrentPrice;
	private Button btnPriceAtOrAbove;
	private Button btnPriceAtOrBelow;
	
	private ModifyListener modifyListenerAbove;
	private ModifyListener modifyListenerBelow;
	
	private Gson gson;
	private HttpRequestFactory requestFactory;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public BTCAlert(Shell parent, int style, Alert btcAlert) {
		super(parent, style);
		setText("BTC Alerts");
		
        gson = new GsonBuilder()
        		//.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
        		.setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        		.registerTypeAdapter(Date.class, new DateDeserializer())
        		.setFieldNamingStrategy(new FieldNamingStrategy() {
			
			@Override
			public String translateName(Field field) {
		        String fieldName = FieldNamingPolicy.IDENTITY.translateName(field);
	            if (field.getDeclaringClass().equals(GetSpotPrice.class) 
	            		|| field.getDeclaringClass().equals(Price.class) 
	            		|| field.getDeclaringClass().isAssignableFrom(AbstractRequest.class)) {
	            	fieldName = FieldNamingPolicy.IDENTITY.translateName(field);
	            	
	            } else
	            	fieldName = FieldNamingPolicy.UPPER_CAMEL_CASE.translateName(field);
	            
	            return fieldName;
			}
		}).create();
        
		requestFactory = new NetHttpTransport().createRequestFactory();
		
		alert = btcAlert;
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		
		// get current btc value
		btcValue = 0;
		try {
			HttpRequest request = requestFactory.buildGetRequest(new GenericUrl("https://api.coinbase.com/v2/prices/BTC-USD/spot"));
			String rawResponse = request.execute().parseAsString();
			GetSpotPrice getSpotPrice = gson.fromJson(rawResponse, GetSpotPrice.class);
	        
			btcValue = getSpotPrice.getData().getAmount();
			//System.out.println("BTC = $" + btcValue);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		final double fBtcValue = btcValue;
		Display.getDefault().asyncExec(new Runnable() {
			 public void run() {
				 textAbove.removeModifyListener(modifyListenerAbove);
				 textBelow.removeModifyListener(modifyListenerBelow);
				 
				 lblCurrentPrice.setText("Current price: $" + df0.format(fBtcValue));
				 if (alert.getAbove() == -1) {
					 textAbove.setText(df0.format(fBtcValue));
					 btnPriceAtOrAbove.setSelection(false);
				 }
				 
				 if (alert.getBelow() == -1) {
					 textBelow.setText(df0.format(fBtcValue));
					 btnPriceAtOrBelow.setSelection(false);
				 }
				 
				if (alert.getAbove() != -1)
					textAbove.setText(df0.format(alert.getAbove()));
				if (alert.getBelow() != -1)
					textBelow.setText(df0.format(alert.getBelow()));
				
				btnPriceAtOrAbove.setSelection(alert.isAboveEnabled());
				btnPriceAtOrBelow.setSelection(alert.isBelowEnabled());
				
				textAbove.addModifyListener(modifyListenerAbove);
				textBelow.addModifyListener(modifyListenerBelow);
			 }
		});
		
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(308, 195);
		shell.setText(getText());
		
		lblCurrentPrice = new Label(shell, SWT.NONE);
		lblCurrentPrice.setBounds(10, 10, 282, 23);
		lblCurrentPrice.setText("Current Price:");
		
		textAbove = new Text(shell, SWT.BORDER);
		textAbove.setBounds(177, 39, 115, 29);
		modifyListenerAbove = new ModifyListener() {
		    public void modifyText(ModifyEvent e) {
	    		alert.setAbove(Double.parseDouble(textAbove.getText()));
	    		if (alert.getAbove() > 0) {
	    			alert.setAboveEnabled(true);
	    			btnPriceAtOrAbove.setSelection(true);
	    		} else {
	    			alert.setAboveEnabled(false);
	    			btnPriceAtOrAbove.setSelection(false);
	    		}
		    }
		};
		textAbove.addModifyListener(modifyListenerAbove);
		
		textBelow = new Text(shell, SWT.BORDER);
		textBelow.setBounds(177, 74, 115, 29);
		modifyListenerBelow = new ModifyListener() {
		    public void modifyText(ModifyEvent e) {
	    		alert.setBelow(Double.parseDouble(textBelow.getText()));
	    		if (alert.getBelow() > 0) {
	    			alert.setBelowEnabled(true);
	    			btnPriceAtOrBelow.setSelection(true);
	    		} else {
	    			alert.setBelowEnabled(false);
	    			btnPriceAtOrBelow.setSelection(false);
	    		}
		    }
		};
		textBelow.addModifyListener(modifyListenerBelow);
		
		Button btnSave = new Button(shell, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = alert;
				shell.close();
			}
		});
		btnSave.setBounds(10, 118, 103, 33);
		btnSave.setText("Save");
		
		Button btnCancel = new Button(shell, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = null;
				shell.close();
			}
		});
		btnCancel.setBounds(189, 118, 103, 33);
		btnCancel.setText("Cancel");
		
		btnPriceAtOrAbove = new Button(shell, SWT.CHECK);
		btnPriceAtOrAbove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				alert.setAboveEnabled(btnPriceAtOrAbove.getSelection());
			}
		});
		btnPriceAtOrAbove.setBounds(10, 39, 161, 23);
		btnPriceAtOrAbove.setText("Price at or Above:");
		
		btnPriceAtOrBelow = new Button(shell, SWT.CHECK);
		btnPriceAtOrBelow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				alert.setBelowEnabled(btnPriceAtOrBelow.getSelection());
			}
		});
		btnPriceAtOrBelow.setBounds(10, 72, 161, 23);
		btnPriceAtOrBelow.setText("Price at or Below:");

	}
}
