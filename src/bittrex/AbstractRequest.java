package bittrex;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Map;

import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import coinbase.GetSpotPrice;
import coinbase.Price;
import gson.DateDeserializer;

public class AbstractRequest {
	
	protected static Gson gson;
	protected static HttpRequestFactory requestFactory;
	
	static {
        gson = new GsonBuilder()
        		//.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
        		.setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        		.registerTypeAdapter(Date.class, new DateDeserializer())
        		.setFieldNamingStrategy(new FieldNamingStrategy() {
			
			@Override
			public String translateName(Field field) {
		        String fieldName = FieldNamingPolicy.IDENTITY.translateName(field);
	            if (field.getDeclaringClass().equals(GetSpotPrice.class) 
	            		|| field.getDeclaringClass().equals(Price.class) 
	            		|| field.getDeclaringClass().equals(GetMarkets.class)
	            		|| field.getDeclaringClass().equals(GetMarketSummaries.class)
	            		|| field.getDeclaringClass().equals(GetTicker.class)
	            		|| field.getDeclaringClass().equals(GetMarketHistory.class)){
	            	fieldName = FieldNamingPolicy.IDENTITY.translateName(field);
	            	
	            } else
	            	fieldName = FieldNamingPolicy.UPPER_CAMEL_CASE.translateName(field);
	            
	            return fieldName;
			}
		}).create();
        
		requestFactory = new NetHttpTransport().createRequestFactory();
	}
	
	public static String fixFullUrl(String baseUrl, Map<String, String> parameters) {
		String url = baseUrl;
		if (parameters != null) {
			url += "?";
			int i = 0;
			for (String key : parameters.keySet()) {
				String value = parameters.get(key);
				url += key + "=" + value;
				if (i < parameters.size() - 1)
					url += "&";
				
				i++;
			}
		}
		return url;
	}
}
