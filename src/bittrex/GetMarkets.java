package bittrex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;

public class GetMarkets {
	
	private String baseUrl;
	private Map<String, String> parameters;
	private String fullUrl;

	private boolean success;
	private String message;
	private ArrayList<Market> result;
	
	public GetMarkets() {
		this.baseUrl = "https://bittrex.com/api/v1.1/public/getmarkets";
		this.parameters = null;
		this.fullUrl = AbstractRequest.fixFullUrl(baseUrl, parameters);
	}
	
	public void run() {
		try {
			HttpRequest request = AbstractRequest.requestFactory.buildGetRequest(new GenericUrl(fullUrl));
			String rawResponse = request.execute().parseAsString();
	        GetMarkets abstractRequest = (GetMarkets) AbstractRequest.gson.fromJson(rawResponse, GetMarkets.class);
	        this.message = abstractRequest.message;
	        this.success = abstractRequest.success;
	        this.result = abstractRequest.result;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<Market> getResult() {
		return result;
	}

	public void setResult(ArrayList<Market> result) {
		this.result = result;
	}
	
}
