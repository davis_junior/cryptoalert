package bittrex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;

public class GetMarketSummaries {
	
	private String baseUrl;
	private Map<String, String> parameters;
	private String fullUrl;
	
	private boolean success;
	private String message;
	private ArrayList<MarketSummary> result;
	
	public GetMarketSummaries() {
		this.baseUrl = "https://bittrex.com/api/v1.1/public/getmarketsummaries";
		this.parameters = null;
		this.fullUrl = AbstractRequest.fixFullUrl(baseUrl, parameters);
	}
	
	public void run() {
		try {
			HttpRequest request = AbstractRequest.requestFactory.buildGetRequest(new GenericUrl(fullUrl));
			String rawResponse = request.execute().parseAsString();
			GetMarketSummaries abstractRequest = (GetMarketSummaries) AbstractRequest.gson.fromJson(rawResponse, GetMarketSummaries.class);
	        this.message = abstractRequest.message;
	        this.success = abstractRequest.success;
	        this.result = abstractRequest.result;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<MarketSummary> getResult() {
		return result;
	}

	public void setResult(ArrayList<MarketSummary> result) {
		this.result = result;
	}
	
}
