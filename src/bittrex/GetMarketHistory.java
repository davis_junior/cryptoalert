package bittrex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;

public class GetMarketHistory {
	
	private String baseUrl;
	private Map<String, String> parameters;
	private String fullUrl;

	private boolean success;
	private String message;
	private ArrayList<MarketHistory> result;
	
	public GetMarketHistory(String market) {
		this.baseUrl = "https://bittrex.com/api/v1.1/public/getmarkethistory";
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("market", market);
		this.parameters = parameters;
		this.fullUrl = AbstractRequest.fixFullUrl(baseUrl, parameters);
	}
	
	public void run() {
		try {
			HttpRequest request = AbstractRequest.requestFactory.buildGetRequest(new GenericUrl(fullUrl));
			String rawResponse = request.execute().parseAsString();
	        GetMarketHistory abstractRequest = (GetMarketHistory) AbstractRequest.gson.fromJson(rawResponse, GetMarketHistory.class);
	        this.message = abstractRequest.message;
	        this.success = abstractRequest.success;
	        this.result = abstractRequest.result;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<MarketHistory> getResult() {
		return result;
	}

	public void setResult(ArrayList<MarketHistory> result) {
		this.result = result;
	}
	
}
