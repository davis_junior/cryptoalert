package bittrex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;

public class GetTicker {
	
	private String baseUrl;
	private Map<String, String> parameters;
	private String fullUrl;
	
	private boolean success;
	private String message;
	private Ticker result;
	
	public GetTicker(String market) {
		this.baseUrl = "https://bittrex.com/api/v1.1/public/getticker";
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("market", market);
		this.parameters = parameters;
		this.fullUrl = AbstractRequest.fixFullUrl(baseUrl, parameters);
	}
	
	public void run() {
		try {
			HttpRequest request = AbstractRequest.requestFactory.buildGetRequest(new GenericUrl(fullUrl));
			String rawResponse = request.execute().parseAsString();
			GetTicker abstractRequest = (GetTicker) AbstractRequest.gson.fromJson(rawResponse, GetTicker.class);
	        this.message = abstractRequest.message;
	        this.success = abstractRequest.success;
	        this.result = abstractRequest.result;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Ticker getResult() {
		return result;
	}

	public void setResult(Ticker result) {
		this.result = result;
	}
}
