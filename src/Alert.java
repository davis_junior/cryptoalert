import java.util.Date;

public class Alert {

	private String marketName;
	private String marketCurrencyLong;
	private double above;
	private double below;
	private boolean aboveEnabled;
	private boolean belowEnabled;
	private long aboveTimestamp;
	private long belowTimestamp;
	
	public Alert() {
		marketName = null;
		marketCurrencyLong = null;
		above = -1;
		below = -1;
		aboveEnabled = false;
		belowEnabled = false;
		aboveTimestamp = -1;
		belowTimestamp = -1;
	}
	
	public Alert(String marketName, String marketCurrencyLong, double above, double below,
			boolean aboveEnabled, boolean belowEnabled) {
		
		this.marketName = marketName;
		this.marketCurrencyLong = marketCurrencyLong;
		this.above = above;
		this.below = below;
		this.aboveEnabled = aboveEnabled;
		this.belowEnabled = belowEnabled;
		
		this.aboveTimestamp = -1;
		this.belowTimestamp = -1;
	}
	
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public String getMarketCurrencyLong() {
		return marketCurrencyLong;
	}
	public void setMarketCurrencyLong(String marketCurrencyLong) {
		this.marketCurrencyLong = marketCurrencyLong;
	}
	public double getAbove() {
		return above;
	}
	public void setAbove(double above) {
		this.above = above;
	}
	public double getBelow() {
		return below;
	}
	public void setBelow(double below) {
		this.below = below;
	}
	public boolean isAboveEnabled() {
		return aboveEnabled;
	}
	public void setAboveEnabled(boolean aboveEnabled) {
		this.aboveEnabled = aboveEnabled;
	}
	public boolean isBelowEnabled() {
		return belowEnabled;
	}
	public void setBelowEnabled(boolean belowEnabled) {
		this.belowEnabled = belowEnabled;
	}
	public long getAboveTimestamp() {
		return aboveTimestamp;
	}
	public void setAboveTimestamp(long aboveTimestamp) {
		this.aboveTimestamp = aboveTimestamp;
	}
	public long getBelowTimestamp() {
		return belowTimestamp;
	}
	public void setBelowTimestamp(long belowTimestamp) {
		this.belowTimestamp = belowTimestamp;
	}
	
}
